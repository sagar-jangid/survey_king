# from django import forms
# from django.forms.widgets import RadioSelect, Textarea
# from models import Question, Survey, Options, QuestionType
#
#
# class QuestionForm(forms.Form):
#     def __init__(self, user, survey_name, *args, **kwargs):
#         super(QuestionForm, self).__init__(*args, **kwargs)
#         question = Question.objects.filter(survey=(Survey.objects.filter(title=survey_name))[0], user=user)
#         for q in question:
#             question_type = QuestionType.objects.filter(id=Question.objects.filter(text=q).values_list('question_type'))
#             if question_type == "Single Textbox":
#                 pass
#
#             if question_type == "Multiple Choice":
#
#                 choices = Options.objects.all().filter(
#                     question=Question.objects.filter(survey=(Survey.objects.filter(title=survey_name))[0],
#                                                      text= q)).values_list('option_text')
#                 choice_list = [option[0].encode("ascii") for option in choices]  # unicode to ascii conversion
#
#                 self.fields["answers"] = forms.ChoiceField(choices=choice_list,
#                                                            widget=RadioSelect)
#
#             if question_type == "Multiple Text Box":
#                 pass
#
#             if question_type == "Check Box":
#                 pass
#
