# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


# Create your models here.
class Category(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=400)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    # survey = models.ForeignKey(Survey)
    #
    def __unicode__(self):
        return (self.name)
# #
# category1 = Category(name="Customer Feedback")
# category2 = Category(name="Just For Fun")
# category3 = Category(name="Employee Feedback")
# category4 = Category(name="On Board Survey")
# category1.save()
# category2.save()
# category3.save()
# category4.save()




class Survey(models.Model):
    objects = models.Manager()
    user = models.ForeignKey(User)
    title = models.CharField(max_length=400)
    category = models.ForeignKey(Category, null=True)
    # category = models.CharField(max_length=400)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return (self.title)

    # def questions(self):
    #     if self.pk:
    #         return Question.objects.filter(survey=self.pk)
    #     else:
    #         return None


def validate_list(value):
    '''takes a text value and verifies that there is at least one comma '''
    values = value.split(',')
    if len(values) < 2:
        raise ValidationError(
            "The selected field requires an associated list of choices. Choices must contain more than one item.")


class QuestionType(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=400)
    created = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return (self.name)

# qt1 = QuestionType(name="Multiple Choice" )
# qt2 = QuestionType(name="Check Box" )
# qt3 = QuestionType(name="Multiple Text Box" )
# qt4 = QuestionType(name="Single Textbox" )
# qt1.save()
# qt2.save()
# qt3.save()
# qt4.save()


class Question(models.Model):
    objects = models.Manager()
    user = models.ForeignKey(User)
    text = models.TextField()
    required = models.BooleanField(default=True)
    # category = models.ForeignKey(Category, blank=True, null=True, )
    survey = models.ForeignKey(Survey)
    # question_type = models.CharField(max_length=200, choices=QUESTION_TYPES, default=TEXT)
    question_type = models.ForeignKey(QuestionType)
    # the choices field is only used if the question type
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return (self.text)

    # def save(self, *args, **kwargs):
    #     if (self.question_type == Question.RADIO or self.question_type == Question.SELECT
    #             or self.question_type == Question.SELECT_MULTIPLE):
    #         validate_list(self.choices)
    #     super(Question, self).save(*args, **kwargs)

    # def get_choices(self):
    #     ''' parse the choices field and return a tuple formatted appropriately
    #     for the 'choices' argument of a form widget.'''
    #     choices = self.choices.split(',')
    #     choices_list = []
    #     for c in choices:
    #         c = c.strip()
    #         choices_list.append((c, c))
    #     choices_tuple = tuple(choices_list)
    #     return choices_tuple



class Options(models.Model):
    objects = models.Manager()
    question = models.ForeignKey(Question)
    user = models.ForeignKey(User, null=True)
    survey = models.ForeignKey(Survey, null=True)
    option_text = models.CharField(max_length=800)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return (self.option_text)


class Response(models.Model):
    # a response object is just a collection of questions and answers with a
    # unique interview uuid
    objects = models.Manager()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    survey = models.ForeignKey(Survey)
    respondent_ip = models.CharField('IP of respondent', max_length=400, default="127.0.0.1")
    question = models.ForeignKey(Question)
    answer_option = models.TextField(blank=True, null=True)
    # interviewee = models.CharField('Name of Interviewee', max_length=400)
    # conditions = models.TextField('Conditions during interview', blank=True, null=True)
    # comments = models.TextField('Any additional Comments', blank=True, null=True)
    # interview_uuid = models.CharField("Interview unique identifier", max_length=36)

    # def __unicode__(self):
    #     return ("response %s" % self.interview_uuid)


# class AnswerBase(models.Model):
#     question = models.ForeignKey(Question)
#     response = models.ForeignKey(Response)
#     created = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)
#
#
# # these type-specific answer models use a text field to allow for flexible
# # field sizes depending on the actual question this answer corresponds to. any
# # "required" attribute will be enforced by the form.
# class AnswerText(AnswerBase):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerRadio(AnswerBase):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerSelect(AnswerBase):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerSelectMultiple(AnswerBase):
#     body = models.TextField(blank=True, null=True)
