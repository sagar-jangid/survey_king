from django.conf.urls import url
from views import create_survey, get_create_survey_data, store_survey_question, preview_survey_question, \
    get_responses_by_email, collect_responses, get_responses_by_weblink,get_data_for_email_response, \
    store_survey_responses, survey_selection_for_response, show_response_for_selected_survey

urlpatterns = [
    url(r'^create_survey/$', create_survey, name='create_survey'),
    url(r'^get_create_survey_data/$', get_create_survey_data, name='get_create_survey_data'),
    url(r'^store_survey_question/$', store_survey_question, name='store_survey_question'),
    url(r'^preview_survey_question/$', preview_survey_question, name='preview_survey_question'),
    url(r'^get_responses_by_email/$', get_responses_by_email, name='get_responses_by_email'),
    url(r'^get_responses_by_weblink/$', get_responses_by_weblink, name='get_responses_by_link'),
    url(r'^collect_responses/$', collect_responses, name='collect_responses'),
    url(r'^get_data_for_email_response/$', get_data_for_email_response, name='get_data_for_email_response'),
    url(r'^store_survey_responses/$', store_survey_responses, name='store_survey_responses'),
    url(r'^survey_selection_for_response/$', survey_selection_for_response, name='survey_selection_for_response'),
    url(r'^show_response_for_selected_survey/$', show_response_for_selected_survey, name='show_response_for_selected_survey'),

]
