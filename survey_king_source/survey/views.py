# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from Crypto.Cipher import AES
import base64
from survey_king.settings import secret_key
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from models import Category, Survey, QuestionType, Question, Options, Response
from django.core.mail import send_mail
from graphos.sources.simple import SimpleDataSource
from graphos.renderers.gchart import LineChart, PieChart, BarChart
from graphos.sources.model import ModelDataSource
from collections import Counter
import urllib

# Create your views here.

survey_name = ""
question_type = ""


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@login_required
def create_survey(request):
    category = Category.objects.all()
    return render(request, 'survey/create_survey.html', {'category': category})


@login_required
def get_create_survey_data(request):
    global question_type
    question_type = QuestionType.objects.all()
    if request.user.is_authenticated():
        global survey_name
        survey_name = request.POST['survey_name'] if 'survey_name' in request.POST else survey_name
        if 'select_survey_dropdown' in request.POST and survey_name != '':
            survey_category = request.POST['select_survey_dropdown']
            new_survey = Survey(user=request.user, title=survey_name,
                                category=(Category.objects.filter(name=survey_category))[0])
            new_survey.save()
            return render(request, 'survey/survey_question.html',
                          {'question_type': question_type, 'survey_name': survey_name})
    return render(request, 'survey/survey_question.html', {'question_type': question_type, 'survey_name': survey_name})


@login_required
def store_survey_question(request):
    if 'select_question' in request.POST:
        if request.user.is_authenticated():
            if request.POST['select_question'] == 'Check Box':
                print request.POST
                print Survey.objects.filter(title=survey_name)
                check_box_question = Question(user=request.user, text=request.POST['question_text'],
                                              survey=(Survey.objects.filter(title=survey_name))[0],
                                              question_type=(QuestionType.objects.filter(
                                                  name=request.POST['select_question']))[0])
                check_box_question.save()
                option1 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['checkboxoption1'])
                option2 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['checkboxoption2'])
                option3 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['checkboxoption3'])
                option4 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['checkboxoption4'])
                option1.save()
                option2.save()
                option3.save()
                option4.save()

            if request.POST['select_question'] == 'Multiple Choice':
                multiple_choice_question = Question(user=request.user, text=request.POST['question_text'],
                                                    survey=(Survey.objects.filter(title=survey_name))[0],
                                                    question_type=(QuestionType.objects.filter(
                                                        name=request.POST['select_question']))[0])
                multiple_choice_question.save()
                option1 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['radiooption1'])
                option2 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['radiooption2'])
                option3 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['radiooption3'])
                option4 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['radiooption4'])
                option1.save()
                option2.save()
                option3.save()
                option4.save()
            if request.POST['select_question'] == 'Multiple Text Box':
                multiple_textbox_question = Question(user=request.user, text=request.POST['question_text'],
                                                     survey=(Survey.objects.filter(title=survey_name))[0],
                                                     question_type=(QuestionType.objects.filter(
                                                         name=request.POST['select_question']))[0])
                multiple_textbox_question.save()
                option1 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['mtextboxoption1'])
                option2 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['mtextboxoption2'])
                option3 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['mtextboxoption3'])
                option4 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['mtextboxoption4'])
                option1.save()
                option2.save()
                option3.save()
                option4.save()
            if request.POST['select_question'] == 'Single Textbox':
                single_textbox_question = Question(user=request.user, text=request.POST['question_text'],
                                                   survey=(Survey.objects.filter(title=survey_name))[0],
                                                   question_type=(QuestionType.objects.filter(
                                                       name=request.POST['select_question']))[0])
                single_textbox_question.save()
                option1 = Options(question=(Question.objects.filter(
                    text=request.POST['question_text'], survey=(Survey.objects.filter(title=survey_name))[0],
                    user=request.user))[0], survey=(Survey.objects.filter(title=survey_name))[0], user=request.user,
                                  option_text=request.POST['singletextboxoption1'])
                option1.save()

            survey_question = request.POST['question_text']
            print survey_question
            # return HttpResponse("hi")
            # return redirect(request.META.get('HTTP_REFERER'),{'survey_name': survey_name})
            print "redirecting"
            return redirect(request.get_full_path())
    else:
        while ('select_question' not in request.POST):
            pass


# question = Question.objects.filter(survey=(Survey.objects.filter(title="Ganesha"))[0])
# choice_array = []
# question_list = []
# for q in question:
#     print q
#     qt = QuestionType.objects.filter(id=Question.objects.filter(text=q).values_list('question_type'))[0]
#     question_list.append([str(q),str(qt)])
#     choices = Options.objects.all().filter(
#     question=Question.objects.filter(text=q)).values_list('option_text')
#     choice_list = [option[0].encode("ascii") for option in choices]  # unicode to ascii conversion is also done
#     choice_array.append(choice_list)
# print choice_array
# print question_list
# for i in question_list:
#     if (i[1] == "Multiple Choice"):
#         print i[0]

# qt=QuestionType.objects.filter(id = Question.objects.filter(text="What is your name?").values_list('question_type'))
# print qt[0]

@login_required
def preview_survey_question(request):
    question = Question.objects.filter(survey=(Survey.objects.filter(title=survey_name))[0], user=request.user)
    choice_array = []
    question_list = []
    for q in question:
        print q
        print survey_name
        print Question.objects.filter(text=q, survey=(Survey.objects.filter(title=survey_name))[0],
                                      user=request.user).values_list('question_type')
        qt = QuestionType.objects.filter(
            id=Question.objects.filter(text=q, survey=(Survey.objects.filter(title=survey_name))[0],
                                       user=request.user).values_list('question_type'))[0]
        print "qt", qt

        question_list.append([str(q), str(qt)])
        choices = Options.objects.all().filter(
            question=Question.objects.filter(text=q, survey=(Survey.objects.filter(title=survey_name))[0],
                                             user=request.user), survey=(Survey.objects.filter(title=survey_name))[0],
            user=request.user).values_list('option_text')
        choice_list = [option[0].encode("ascii") for option in choices]  # unicode to ascii conversion is also done
        choice_array.append([str(q), choice_list])
    print choice_array
    return render(request, 'survey/preview_survey_questions.html',
                  {'question_list': question_list, 'choice_list': choice_array, 'survey_name': survey_name})


cipher = AES.new(secret_key, AES.MODE_ECB)  # never use ECB in strong systems obviously


@login_required()
def collect_responses(request):
    return render(request, 'survey/collect_responses.html', {'survey_name': survey_name})


@login_required()
def get_responses_by_weblink(request):
    survey_id = str(Survey.objects.filter(title=survey_name, user=request.user).values_list('id')[0][0])

    if survey_id.__len__() == 1:
        survey_id = "000000000000000" + survey_id
    elif survey_id.__len__() == 2:
        survey_id = "00000000000000" + survey_id
    else:
        survey_id = "0000000000000" + survey_id

    encoded = base64.b64encode(cipher.encrypt(survey_id))
    encoded = encoded.replace("/", "_")
    survey_response_url = request.get_host() + "/r/" + encoded
    return render(request, 'survey/get_responses_by_weblink.html',
                  {'survey_name': survey_name, 'survey_link': survey_response_url})


success_message = ""


@login_required()
def get_responses_by_email(request):
    return render(request, 'survey/get_responses_by_email.html', {'survey_name': survey_name})


def get_data_for_email_response(request):
    success_message = "Your respondents are successfully sent a message containing survey link."
    subject = request.POST['subject']
    message = request.POST['message']
    emails = request.POST['emails']
    email_list = [email.encode("ascii") for email in emails.split(",")]

    survey_id = str(Survey.objects.filter(title=survey_name, user=request.user).values_list('id')[0][0])

    if survey_id.__len__() == 1:
        survey_id = "000000000000000" + survey_id
    elif survey_id.__len__() == 2:
        survey_id = "00000000000000" + survey_id
    else:
        survey_id = "0000000000000" + survey_id

    encoded = base64.b64encode(cipher.encrypt(survey_id))
    encoded = encoded.replace("/", "_")
    survey_response_url = request.get_host() + "/r/" + encoded

    html_message = '<p>' + message + '</p><br><a href="' + survey_response_url + '" style="border-radius: 5px;\
    border: 5px solid #00BF6F ;\
    width : 375px;\
    height:80px;\
    text-decoration:none;\
    background-color: #00BF6F;\
    font-size: 15px;\
    font-family: sans-serif;\
    font-weight: bold;\
    color: white; " >' + "click Here" + "</a>"

    print html_message
    send_mail(subject, message, 'surveykingitt@gmail.com',
              email_list, html_message=html_message)

    return render(request, 'survey/get_responses_by_email.html',
                  {'survey_name': survey_name, 'success_message': success_message})


def view_survey_for_response(request, encrypted_survey_id):
    decoded = cipher.decrypt(base64.b64decode(encrypted_survey_id.replace("_", "/")))
    decoded = decoded.lstrip("0")
    print decoded

    question = Question.objects.filter(survey=(Survey.objects.filter(id=decoded))[0])
    choice_array = []
    question_list = []
    for q in question:
        print q
        print survey_name
        print Question.objects.filter(text=q, survey=(Survey.objects.filter(id=decoded))[0]).values_list('question_type')
        qt = QuestionType.objects.filter(
            id=Question.objects.filter(text=q, survey=(Survey.objects.filter(id=decoded))[0]).values_list('question_type'))[0]
        print "qt", qt

        question_list.append([str(q), str(qt)])
        choices = Options.objects.all().filter(
            question=Question.objects.filter(text=q, survey=(Survey.objects.filter(id=decoded))[0]), survey=(Survey.objects.filter(id=decoded))[0]).values_list('option_text')
        choice_list = [option[0].encode("ascii") for option in choices]  # unicode to ascii conversion is also done
        choice_array.append([str(q), choice_list])
    print choice_array
    return render(request, 'survey/survey_response_form.html',
                  {'question_list': question_list, 'choice_list': choice_array,
                   'survey_name': Survey.objects.filter(id=decoded).values_list('title')[0][0]})


def store_survey_responses(request):
    url = urllib.unquote(request.META.get('HTTP_REFERER'))
    url = str(url)
    a = [component for component in url.split('/')]
    decoded = cipher.decrypt(base64.b64decode(a[4].replace("_", "/")))
    decoded = decoded.lstrip("0")
    survey_questions = Question.objects.filter(survey=(Survey.objects.filter(id=decoded))[0]).values_list('text')
    survey_question_list = []
    for q in survey_questions:
        survey_question_list.append(q[0].encode("ascii"))

    for question in survey_question_list:

        qt = QuestionType.objects.filter(
            id=Question.objects.filter(text=question, survey=(Survey.objects.filter(id=decoded))[0]).values_list(
                'question_type'))[0]

        qt = str(qt)

        if qt == "Multiple Choice":
            print "Hi"
            response = Response(survey=(Survey.objects.filter(id=decoded))[0],
                                question=Question.objects.filter(text=question,
                                                                 survey=(Survey.objects.filter(id=decoded))[0])[0],
                                respondent_ip=get_client_ip(request),
                                answer_option=request.POST[question].encode('ascii'))
            response.save()

        if qt == "Check Box":
            answer = request.POST.getlist('options' + question)
            answer_list = [element.encode('ascii') for element in answer]
            answer_string = ',,'.join(answer_list)

            response = Response(survey=(Survey.objects.filter(id=decoded))[0],
                                question=Question.objects.filter(text=question,
                                                                 survey=(Survey.objects.filter(id=decoded))[0])[0],
                                respondent_ip=get_client_ip(request),
                                answer_option=answer_string)
            response.save()

        if qt == "Multiple Text Box":
            multiple_choice_post_data = []
            for i in range(1, 5):
                multiple_choice_post_data.append((request.POST['multipletextbox' + question + str(i)]).encode('ascii'))

            multiple_choice_answer = ",,".join(multiple_choice_post_data)
            response = Response(survey=(Survey.objects.filter(id=decoded))[0],
                                question=Question.objects.filter(text=question,
                                                                 survey=(Survey.objects.filter(id=decoded))[0])[0],
                                respondent_ip=get_client_ip(request),
                                answer_option=multiple_choice_answer)
            response.save()

        if qt == "Single Textbox":
            response = Response(survey=(Survey.objects.filter(id=decoded))[0],
                                question=Question.objects.filter(text=question,
                                                                 survey=(Survey.objects.filter(id=decoded))[0])[0],
                                respondent_ip=get_client_ip(request),
                                answer_option=request.POST['singletextbox' + question].encode('ascii'))
            response.save()

    return render(request, 'survey/thank_you.html') #This was the issue


def survey_selection_for_response(request):
    survey = Survey.objects.filter(user=request.user).values_list('id', 'title')
    responsed_survey= []
    for entry in survey:
        temp_responsed_survey = Response.objects.filter(survey=entry[0])
        if not temp_responsed_survey:
            pass
        else:
            responsed_survey.append(entry[1].encode('ascii'))

    print responsed_survey
        # survey = [title[0].encode('ascii') for title in survey]
    return render(request, 'survey/survey_selection_for_response.html', {'survey': responsed_survey})


def show_response_for_selected_survey(request):
    survey_id = Survey.objects.filter(user=request.user, title=request.POST['selected_survey']).values_list('id')
    survey_questions = Question.objects.filter(survey=(Survey.objects.filter(id=survey_id))[0]).values_list('id')

    data_return_list = []
    checkbox_question_count = 0
    radio_question_count = 0

    for question in survey_questions:

        qt = QuestionType.objects.filter(id=Question.objects.filter(id=question[0]).values_list('question_type'))[0]
        qt = str(qt)

        if qt == "Multiple Choice":
            responses = Response.objects.filter(question=question[0]).values_list('answer_option')
            options = Options.objects.filter(question=question[0]).values_list('option_text')
            options_list = []
            for option in options:
                options_list.append(option[0].encode('ascii'))

            response_list = []
            for response in responses:
                response_list.append(response[0].encode('ascii'))
            print response_list

            # response_list += options_list
            print response_list

            colors = ['red', 'green', 'blue', 'silver']
            response_count = Counter(response_list)
            print response_count
            data = [
                ['Name', 'Response Percentage', {'role': 'style'}],
            ]
            question_text = (Question.objects.filter(id=question[0]).values_list('text')[0][0]).encode("ascii")
            index = 0
            total = sum(response_count.values())
            print total
            for option_value in options_list:
                if option_value in response_count.keys():
                    print option_value
                    print response_count[option_value]
                    percent = (response_count[option_value]/float(total))*100
                    print percent
                    temp_data = [option_value, percent,colors[index]]
                else:
                    print "else"+option_value
                    temp_data = [option_value, 0, colors[index]]
                index +=1
                data.append(temp_data)

            # queryset = Response.objects.filter(question=question[0])
            # data_source = ModelDataSource(queryset,
            #                               fields=['answer_option'])

            data_source = SimpleDataSource(data=data)
            chart_radio = BarChart(data_source, options={'legend':'none'})
            context_radio = {'chart_radio' + str(radio_question_count): chart_radio}
            radio_question_count += 1
            data_return_list.append([qt, question_text, {'chart': chart_radio}])
            # return render(request, 'survey/show_responses.html', context_radio)

        if qt == "Check Box":
            question_text = (Question.objects.filter(id=question[0]).values_list('text')[0][0]).encode("ascii")
            checkbox_responses = Response.objects.filter(question=question[0]).values_list('answer_option')
            options = Options.objects.filter(question=question[0]).values_list('option_text')
            options_list = []
            for option in options:
                options_list.append(option[0].encode('ascii'))

            merged_response_list = []
            for response in checkbox_responses:
                temp_list = response[0].split(',,')
                for element in temp_list:
                    element = element.encode('ascii')
                    merged_response_list.append(element)

            merged_response_list += options_list
            response_count = Counter(merged_response_list)
            data = [
                ['Name', 'Value'],

            ]
            # colors = ['red', 'green', 'blue', 'silver']
            # index = 0
            for answers in range(response_count.keys().__len__()):
                temp_data = [response_count.keys()[answers], response_count[response_count.keys()[answers]]-1]
                # index += 1
                data.append(temp_data)
            print data
            # queryset = Response.objects.filter(question=question[0])
            # data_source = ModelDataSource(queryset,
            #                               fields=['answer_option'])

            data_source = SimpleDataSource(data=data)
            # chart = BarChart(data_source)
            chart_checkbox = PieChart(data_source)
            context_checkbox = {'chart_checkbox' + str(checkbox_question_count): chart_checkbox}
            data_return_list.append([qt, question_text, {'chart': chart_checkbox}])
            # return render(request, 'survey/show_responses.html', context)

        if qt == "Multiple Text Box":
            question_text = (Question.objects.filter(id=question[0]).values_list('text')[0][0]).encode("ascii")
            responses_multiple_choice = Response.objects.filter(question=question[0]).values_list('answer_option')
            multiple_choice_response_list = []
            for records in responses_multiple_choice:
                temp_multiple_choice_response_list = [i.encode("ascii") for i in records[0].split(",,")]
                multiple_choice_response_list.append(temp_multiple_choice_response_list)
            data_return_list.append([qt, question_text, multiple_choice_response_list])

        if qt == "Single Textbox":
            question_text = (Question.objects.filter(id=question[0]).values_list('text')[0][0]).encode("ascii")
            single_textbox_responses = Response.objects.filter(question=question[0]).values_list('answer_option')
            single_textbox_responses_list = []
            for records in single_textbox_responses:
                single_textbox_responses_list.append(records[0].encode('ascii'))
            data_return_list.append([qt, question_text, single_textbox_responses_list])
    print data_return_list
    return render(request, 'survey/show_responses.html',
                  {'survey': request.POST['selected_survey'], 'data_list': data_return_list})
