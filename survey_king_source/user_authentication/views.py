from django.http import HttpResponse
from django.contrib.auth import logout
from .forms import SignUpForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect

from .tokens import account_activation_token


def welcome(request):
    return HttpResponse("Hello World !! First APP")


# def index(request):
#     template = loader.get_template("/basic_login.html")
#     return HttpResponse(template.render())

def home(request):
    return render(request, 'home.html')


# def register(request):
#     # return render(request, 'signup.html')
#     if request.method == 'POST':
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             raw_password = form.cleaned_data.get('password1')
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect('home')
#     else:
#         form = SignUpForm()
#     return render(request, 'signup.html', {'form': form})


def register(request):
    if request.user.is_authenticated():
        return redirect('/dashboard')

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            current_site = get_current_site(request)
            subject = 'Activate Your Survey King Account'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)

            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')


def account_activation_thankyou(request):
    return render(request, 'account_activate_thankyou.html')


def activate(request, uidb64, token, backend='django.contrib.auth.backends.ModelBackend'):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        # login(request, user)
        return redirect('account_activation_thankyou')
    else:
        return render(request, 'account_activation_invalid.html')


@login_required
def dashboard(request):
    return render(request, 'dashboard.html')


@login_required
def logout_user(request):
    logout(request)
    return redirect('home')


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, form.user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            # logout(request)
            # return redirect('password_reset_complete')
            return redirect('change_password')
        else:
            # messages.error(request, 'Please correct the error below.')
            pass
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })

# def clean_email(self):
#     email = self.cleaned_data.get('email')
#     username = self.cleaned_data.get('username')
#     print User.objects.filter(email=email).count()
#     if email and User.objects.filter(email=email).count() > 0:
#         raise forms.ValidationError(u'This email address is already registered.')
#     return email