"""survey_king URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from django.contrib.auth.views import login
from user_authentication.views import home, welcome, register, dashboard, logout_user, account_activation_sent, \
    activate, account_activation_thankyou, change_password
from survey.views import view_survey_for_response

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'welcome', welcome),
    url(r'^login/$', login, name='login', kwargs={'redirect_authenticated_user': True}),
    url(r'^logout_user/$', logout_user, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^$', home, name='home'),
    url(r'^register$', register),
    url(r'^dashboard$', dashboard),
    url(r'^account_activation_thankyou/$', account_activation_thankyou, name='account_activation_thankyou'),
    url(r'^account_activation_sent/$', account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),

    url(r'^password_reset/$', views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', views.password_reset_complete, name='password_reset_complete'),

    url('', include('django.contrib.auth.urls', namespace='auth')),
    url('', include('social.apps.django_app.urls', namespace='social_url')),
    url(r'^password/$', change_password, name='change_password'),


    #survey for response here
    url(r'^r/(\S+)/', view_survey_for_response, name = 'view_survey_for_response'),

    # Survey URL HERE
    url(r'^survey/', include('survey.urls')),


    # url(r'login', index)
    # url(r'^welcome', welcome) Check for beginning as welcome
    # $ matches the end of the string

]
